// pages/List/list.js
var utils = require('../../utils/util.js');
var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageSize: 5,
    pageNo: 1,
    inputValue: '',
    list: [],
    loadMore: false,
    noLoad: true
  },
  // 获取列表
  getList: function() {
    // console.log('获取数据');
    var that = this;
    var currentList = that.data.list;
    wx.request({
      url: app.globalData.serverUrl + '/webBlog/api/article/weixinList',
      method: 'POST',
      data: {
        pageQuery: {
          pageNum: that.data.pageNo,
          pageSize: that.data.pageSize,
          total: null
        },
        keyword: '',
        typeId: '',
        tagId: ''
      },
      success: function(result) {
        result = result.data.data;
        var list = result.list;
        for (var i = 0; i < list.length; i++) {
          list[i].crtdtm = utils.formatTime(list[i].crtdtm, 'Y-M-D h:m:s');
          currentList.push(list[i]);
        }
        // console.log(currentList.length + 'pageNo=' + that.data.pageNo);
        if (result == null) {
          var toastText = '获取数据失败';
          wx.showToast({
            title: toastText,
            icon: '',
            duration: 2000
          })
        } else {
          that.setData({
            list: currentList
          });
          that.data.pageNo++;
          //判断是否还有数据需要加载
          if (result.nextPage === 0) {
            that.setData({
              loadMore: true
            });
          }
          //暂无
          if (result.total === 0) {
            that.setData({
              noLoad: false
            });
          }
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {


  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getList();
    wx.request({
      url: app.globalData.serverUrl + '/webBlog/api/webinfo/getInitData',
      method: 'GET',
      data: {},
      success: function (result) {
        // set types and tags storage
        wx.setStorage({
          key: "tagList",
          data: result.data.data.tags
        })
        wx.setStorage({
          key: "categoryList",
          data: result.data.data.types
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.setData({
      list: [],
      pageNo: 1
    });
    this.getList();
    // console.log('下拉刷新。。。。');
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    this.setData({
      loadMore: false
    });
    // console.log('上拉加载。。。。');
    var that = this;
    that.getList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})