// pages/List/list.js
var utils = require('../../utils/util.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    keyword: '',
    typeId: '',
    tagId: '',
    currentCate: 0,
    inputShowed: false,
    inputVal: "",
    pageSize: 5,
    pageNo: 1,
    list: [],
    loadMore: false,
    noLoad: true,
    tagList: [],
    categoryList: []
  },
  //search
  showInput: function() {
    this.setData({
      inputShowed: true
    });
  },
  hideInput: function() {
    this.setData({
      inputVal: "",
      inputShowed: false
    });
  },
  clearInput: function() {
    this.setData({
      inputVal: ""
    });
  },
  // search change
  inputTyping: function(e) {
    this.setData({
      inputVal: e.detail.value,
      keyword: e.detail.value
    });
  },
  //点击分类
  changeCategory: function(e) {
    this.setData({
      currentCate: e.currentTarget.dataset.index,
      typeId: e.currentTarget.dataset.id,
      list: [],
      pageNo: 1
    });
    this.getList();
    // console.log(e.currentTarget.dataset.id);
  },
  //点击标签
  clickTag: function(e) {
    // 如果点击过，尽心取消操作

    this.setData({
      tagId: this.data.tagId == e.currentTarget.dataset.id ? '' : e.currentTarget.dataset.id,
      list: [],
      pageNo: 1,
      inputShowed: false
    });
    // console.log(e.currentTarget.dataset.id);
    this.getList();
  },

  // 获取列表
  getList: function() {
    // console.log('获取数据');
    var that = this;
    var currentList = that.data.list;
    wx.request({
      url: app.globalData.serverUrl + '/webBlog/api/article/weixinList',
      method: 'POST',
      data: {
        pageQuery: {
          pageNum: that.data.pageNo,
          pageSize: that.data.pageSize,
          total: null
        },
        keyword: that.data.keyword,
        typeId: that.data.typeId,
        tagId: that.data.tagId
      },
      success: function(result) {
        result = result.data.data;
        var list = result.list;
        for (var i = 0; i < list.length; i++) {
          // list[i].crtdtm = utils.formatTime(list[i].crtdtm, 'Y-M-D h:m:s');
          currentList.push(list[i]);
        }
        // console.log(currentList.length + 'pageNo=' + that.data.pageNo);
        if (result == null) {
          var toastText = '获取数据失败';
          wx.showToast({
            title: toastText,
            icon: '',
            duration: 2000
          })
        } else {
          that.setData({
            list: currentList
          });
          that.data.pageNo++;
          //判断是否还有数据需要加载
          if (result.nextPage === 0) {
            that.setData({
              loadMore: true
            });
          } else {
            that.setData({
              loadMore: false
            });
          }
          if (result.total === 0) {
            that.setData({
              noLoad: false
            });
          } else {
            that.setData({
              noLoad: true
            });
          }
        }
      }
    })
  },
  // 获取标签list
  getTagAndTypeList: function() {
    var that = this;
    wx.request({
      url: app.globalData.serverUrl + '/webBlog/api/webinfo/getInitData',
      method: 'GET',
      data: {},
      success: function(result) {
        var tagsList = {
          icon: "http://image.changguoblog.com/all",
          name: "全部",
          uid: ''
        };
        result.data.data.types.unshift(tagsList);
        that.setData({
          tagList: result.data.data.tags,
          categoryList: result.data.data.types
        })
        // set types and tags storage

        wx.setStorage({
          key: "tagList",
          data: result.data.data.tags
        })
        wx.setStorage({
          key: "categoryList",
          data: result.data.data.types
        })
      }
    })
  },

  //点击搜索
  searchSubmit: function() {
    this.setData({
      list: [],
      pageNo: 1,
      inputVal: "",
      inputShowed: false
    });
    this.getList();
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getList();
    this.getTagAndTypeList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.setData({
      list: [],
      pageNo: 1
    });
    this.getList();
    // console.log('下拉刷新。。。。');
    wx.stopPullDownRefresh();

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    this.setData({
      loadMore: false
    });
    // console.log('上拉加载。。。。');
    var that = this;
    that.getList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})