// pages/detail/detail.js
const app = getApp();
var utils = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: {
      title: '',
      crtdtm: '',
      tranHtml: {}
    },
    uid: '',
    tagList: [],
    categoryList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.setData({
      uid: options.uid
    })

    wx.getStorage({
      key: 'categoryList',
      success: function(res) {
        that.setData({
          categoryList: res.data
        })
      }
    })
    wx.getStorage({
      key: 'tagList',
      success: function(res) {
        that.setData({
          tagList: res.data
        })
      }
    })



  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    wx.request({
      url: app.globalData.serverUrl + '/webBlog/api/article/lookDetail',
      method: 'POST',
      data: {
        articleId: that.data.uid
      },
      success: function(result) {
        var infoData = result.data.data;
        let tranHtml = app.towxml.toJson(infoData.contentHtml, 'html');
        //设置文档显示主题，默认'light'
        // tranHtml.theme = 'light';
        infoData.tranHtml = tranHtml;
        infoData.crtdtm = utils.strFormatTime(infoData.crtdtm);
        console.log(infoData.crtdtm);
        that.setData({
          info: infoData
        })
      }
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  // /**
  //  * 页面相关事件处理函数--监听用户下拉动作
  //  */
  // onPullDownRefresh: function() {

  // },

  // /**
  //  * 页面上拉触底事件的处理函数
  //  */
  // onReachBottom: function() {

  // },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})